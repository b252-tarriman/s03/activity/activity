CREATE TABLE Users (
    id INT NOT NULL AUTO_INCREMENT,
    email VARCHAR(50) NOT NULL,
    password VARCHAR(50) NOT NULL,  
    datetime_created DATETIME NOT NULL,
    PRIMARY KEY (id)
);
CREATE TABLE Posts (
    id INT NOT NULL AUTO_INCREMENT,
    title VARCHAR(50) NOT NULL,
    content VARCHAR(50) NOT NULL,
    datetime_released DATETIME NOT NULL,
    users_id INT NOT NULL,
    
    PRIMARY KEY (id),
    CONSTRAINT fk_posts_users_id
        FOREIGN KEY (users_id) REFERENCES users(id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT
);


INSERT INTO users (email,password,datetime_created) VALUES ("johnsmith@gmail.com","passwordA","2021-01-01 01:00:00"),("juandelacruz@gmail.com","passwordB","2021-01-01 02:00:00"),
("janesmith@gmail.com","passwordC","2021-01-01 03:00:00"),
("mariadelacruz@gmail.com","passwordD","2021-01-01 04:00:00"),
("johndoe@gmail.com","passwordE","2021-01-01 05:00:00");


INSERT INTO posts (users_id,title,content,datetime_released) VALUES (1,"First Code", "Hello World!","2021-01-02 01:00:00"),(1,"Second Code", "Hello Earth!","2021-01-02 02:00:00"),(2,"Third Code", "Welcome to Mars!","2021-01-02 03:00:00"),(4,"Fourth Code", "Bye bye solar system","2021-01-02 04:00:00");


SELECT * FROM posts WHERE users_id = 1;
SELECT email,datetime_created FROM users;

UPDATE posts SET content = "Hello to the people of the Earth" where id = 2;

DELETE FROM users WHERE email = "johndoe@gmail.com";